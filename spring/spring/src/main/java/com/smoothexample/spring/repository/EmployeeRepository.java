package com.smoothexample.spring.repository;

import com.smoothexample.spring.model.Employee;
import org.springframework.data.repository.CrudRepository;
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
